package com.firstline.cancerprediction;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CancerpredictionApplication {

	public static void main(String[] args) {
		SpringApplication.run(CancerpredictionApplication.class, args);
	}

}
